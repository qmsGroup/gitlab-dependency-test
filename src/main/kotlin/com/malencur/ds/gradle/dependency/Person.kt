package com.malencur.ds.gradle.dependency

data class Person(val name: String, val age: Int)